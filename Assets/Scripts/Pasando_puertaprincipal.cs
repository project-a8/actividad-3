using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pasando_puertaprincipal : MonoBehaviour
{
    public Animator laPuerta;

    private void OnTriggerEnter(Collider other)
    {
        laPuerta.Play("open_door");
    }

    private void OnTriggerExit(Collider other)
    {
        laPuerta.Play("close_door");
    }

}
