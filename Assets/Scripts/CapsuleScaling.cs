using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour {

    [SerializeField]
    private Vector3 axes;            // posibles ejes de escalado
    public float scaleUnits;         // velocidad de escalado     

    // Update is called once per frame
    void Update () {
        // Acotación de los valores de escalado al valor unitario [-1,1]
        axes = CapsuleMovement.ClampVector3(axes);

        // La escala, al contrario que la rotación y el movimiento, es acumulativa
        // lo que quiere decir que debemos añadir el nuevo valor de la escala, el valor anterior.
        transform.localScale += axes * (scaleUnits * Time.deltaTime);
    }
}

