using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo_cajon : MonoBehaviour
{
    public Animator Elcajon;

    private void OnTriggerEnter(Collider other)
    {
        Elcajon.Play("apertura_cajon");
    }

}
