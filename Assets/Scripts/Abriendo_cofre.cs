using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo_cofre : MonoBehaviour
{
        public Animator Elcofre;

    private void OnTriggerEnter(Collider other)
    {
        Elcofre.Play("openChest");
    }
    private void OnTriggerExit(Collider other)
    {
        Elcofre.Play("closeChest");
    }
 
}
