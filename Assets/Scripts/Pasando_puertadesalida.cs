using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pasando_puertadesalida : MonoBehaviour
{
    public Animator laPuerta2;

    private void OnTriggerEnter(Collider other)
    {
        laPuerta2.Play("open_exitdoor");
    }
    private void OnTriggerExit(Collider other)
    {
        laPuerta2.Play("close_exitdoor");
    }

}
